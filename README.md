# AWG Spark fork

This Gitlab repo is connected upstream with the Apache Spark repository in GitHub. Commits are not constantly kept synchronized because only release tags are of interest. This fork is used to create custom builds and deploy them in the Gitlab Docker registry and EOS.

# Custom builds

Builds are triggered from branch tags. They use the internal Gitlab CI, defined with [.gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/README.html) files.

## Last build status

* [v2.1.0:](https://gitlab.cern.ch/awg/spark/blob/v2.1.0-build/README.md) [![build status](https://gitlab.cern.ch/awg/spark/badges/v2.1.0-build/build.svg)](https://gitlab.cern.ch/awg/spark/commits/v2.1.0-build)
* [v2.0.2:](https://gitlab.cern.ch/awg/spark/blob/v2.0.2-build/README.md) [![build status](https://gitlab.cern.ch/awg/spark/badges/v2.0.2-build/build.svg)](https://gitlab.cern.ch/awg/spark/commits/v2.0.2-build)
* [v1.6.3:](https://gitlab.cern.ch/awg/spark/blob/v1.6.3-build/README.md) [![build status](https://gitlab.cern.ch/awg/spark/badges/v1.6.3-build/build.svg)](https://gitlab.cern.ch/awg/spark/commits/v1.6.3-build)

### How to create a new build

Instructions can be found here: [TO_CREATE_NEW_BUILDS.md](https://gitlab.cern.ch/awg/spark/blob/master/TO_CREATE_NEW_BUILDS.md)

## Available builds

In EOS:

* EOS folder: ``/eos/user/a/awgdpl/spark-releases``, management server URL ``root://eosuser.cern.ch``
* XROOTD path: ``root://eosuser.cern.ch://eos/user/a/awgdpl/spark-releases``

CERNBox:

* https://cernbox.cern.ch/index.php/s/DJxusRrpVvCmFOV

Docker registry:

* [https://gitlab.cern.ch/awg/spark/container_registry](https://gitlab.cern.ch/awg/spark/container_registry)

You can find examples in the each build link listed above.

## Docker images

Docker Spark Images are created using the following "base" images:

* [Dockerfile-base-j7](https://gitlab.cern.ch/awg/spark/blob/master/docker/Dockerfile-base-j7) (based on Debian)
* [Dockerfile-base-j8](https://gitlab.cern.ch/awg/spark/blob/master/docker/Dockerfile-base-j8) (based on Debian)
* [Dockerfile-cern-base-j7](https://gitlab.cern.ch/awg/spark/blob/master/docker/Dockerfile-cern-base-j7) (based on CC7)
* [Dockerfile-cern-base-j8](https://gitlab.cern.ch/awg/spark/blob/master/docker/Dockerfile-cern-base-j8) (based on CC7)

Specified [here](https://gitlab.cern.ch/awg/spark/blob/v1.6.3-build/ci/Dockerfile#L1) in the Docker file used by the Gitlab-ci [deploy job](https://gitlab.cern.ch/awg/spark/blob/v1.6.3-build/.gitlab-ci.yml#L32). Spark releases are build with "cern" images.


Built with:
``docker build -t gitlab-registry.cern.ch/awg/spark:base-j7 -f docker/Dockerfile-base-j7 .``
and pushed in [this registry](https://gitlab.cern.ch/awg/spark/container_registry).

N.B. inside CERN you need to add a nameserver in the ``/etc/resolv.conf`` file before each ``apt-get update`` command.

# Apache Spark

Spark is a fast and general cluster computing system for Big Data. It provides
high-level APIs in Scala, Java, Python, and R, and an optimized engine that
supports general computation graphs for data analysis. It also supports a
rich set of higher-level tools including Spark SQL for SQL and DataFrames,
MLlib for machine learning, GraphX for graph processing,
and Spark Streaming for stream processing.

<http://spark.apache.org/>


## Online Documentation

You can find the latest Spark documentation, including a programming
guide, on the [project web page](http://spark.apache.org/documentation.html).
This README file only contains basic setup instructions.