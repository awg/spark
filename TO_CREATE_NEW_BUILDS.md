* Target a branch, say v2.1.0
* ``git checkout v2.1.0``
* If it's a tag: ``git checkout -b v2.1.0-build``
* Add the ``.gitlab-ci.yml`` file (take example from other "-build" branches)
* Add Dockerfiles inside the ``ci`` folder (take example from other "-build" branches)
* Modify the ``make-distribution.sh`` script adding ``--batch-mode`` to the Maven build command, something like ``BUILD_COMMAND=("$MVN" -T 1C clean package --batch-mode -DskipTests $@)``
* Modify README.md with instruction for the current build
* Commit new branch to start automatic build (access "Pipelines" webpage if needs to be manually triggered)
* Add build sticker to README.md on master.